# eResearch at ACU

Aidan Wilson
aidan.wilson@acu.edu.au

---

## Introduction

* IT
* Research Office
* Intersect

Note: Firstly, about me. I am a researcher, a documentary field linguist working with Aboriginal Australian languages. I also have a keen interest in computers, though not from a very young age at all, but quite a long way into my research career. In my work I always had a particular focus on data management, and this enabled me to work in research support, which is an area that I have found my natural home. I’m passionate about helping others and particularly helping researchers do their very best, and using all the technology available to them. I’m also a strong advocate for the role of research in the university among my IT colleagues, who tend to be from the service delivery side of things.

I officially sit within IT as part of the Engagement and Partnerships team, and my title is Research Consultant. But physically I am located in the Office of Research Services where I provide a range of services to both the research office and the university's research community.

Finally, I am Intersect's eResearch Analyst for ACU. Intersect is a non-profit eResearch organisation that was set up to help universities provide researchers with technological solutions, support, advice and training. Intersect also provides some technological solutions to universities including petabyte-scale data storage, cloud computing, high-performance computing, and professional services including software development, statistical and data analysis, and consulting.

---

## eResearch

* Revolution in modern research practice
	* Technology as core to research
	* Not just a facilitator

* Sector-wide disruption
	* Universities, funding bodies, industry
	* Digital literacy now an essential for researchers

Note: So what is eResearch anyway? eResearch is revolutionalising the way that research is done. Rather than technology simply facilitating research, which is still very important and will continue to be so, eResearch sees technology as core to the process of research.
eResearch is disruptive across the entire tertiary sector, and universities, journals, funding bodies, industry all scrambling to stay ahead of the game. 
Individual Researchers themselves also need to stay ahead of the game and embrace eResearch. Basic digital literacy is increasingly a core component of being a modern researcher.

+++

## eResearch

* From manual processing to automation
* From paper questionnaires to online surveys
* Cloud computing, storage, supercomputing
* Tools and techniques to assist data manipulation
* Collecting data automatically from sensors
* Building repeatable workflows using scripting

Taking advantage of advances in computers and mobile devices to fundamentally change how research is done.

Note: Examples:
* From manual processing to automation
	* Rather than running the same statistical analysis over different datasets each time, automate that analysis as a script with some modifiable parameters to run more simply next time.
* From paper questionnaires to online surveys
	* Paper questionnaires have to be scanned or keyboarded. Online surveys, like Qualtrics, removes the gruntwork from the process leaving more time for experimental design or analysis.
* Cloud computing, storage
	* Twitter harvesting for example, should be left running all the time, so running it on a cloud-based virtual machine means you don’t have to have a laptop plugged in all the time
	* Supercormputing, or more properly high-performance computing, can enable you to perform very computationally intensive work without the bottleneck of your own computer.
* Building repeatable workflows using scripting
	* Run your analysis again and again largely automatically after defining it the first time.
* Collecting data automatically from sensors
	* Sensors have been around for a while of course, but with better eResearch methods, the data that comes off them can be more easily captured and analysed, and with a much faster turnaround

eResearch may even generate research ideas that weren’t possible earlier. For example, twitter research
Sport science may fall into this category too. It has existed for decades of course, but with recent innovations in computing, data processing and storage, it's becoming far simpler to do amazing work, and extremely quickly.

---

## Services available

* Storage:
	* file.acu.edu.au
	* CloudStor.aarnet.edu.au
	* space.intersect.org.au

Note: So what sort of tools and services are available to researchers at ACU?
Firstly, in the research data storage space, there are a number of options available to researchers. We tebnd to recommend researchers to use the file.acu.edu.au service first and foremost, as it is managed by the university and there is a very good support available from IT.
Next there is also the CloudStor service which is managed by AARNet, although you use your ACU credentials to access it and so it is secured by your ACU identity. CloudStor also has the advantage over the local file system in that it can be used to collaborate with researchers at other institutions, or even outside of universities entirely. You can create a file or folder and give the right to read or edit that file to anyone with an Australian or New Zealand university affiliation, and anyone else may also be given a collaborator's account. 
Lastly, Intersect also provides data storage, which is optimised for larger collections, again, collaboration is supported here as we use AAF for authentication. However Intersect storage is not free.

+++

## Services available

* Computing services
	* Applications and Virtual Hardware in IT
	* Virtual hardware via Intersect
	* High performance computing via Intersect

+++

## Services available

* Surveys/data collection tools
	* Qualtrics
	* REDCap

+++

## Services available

* Software tools
	* R, Python, MATLAB, Julia
	* SPSS
	* Nvivo
	* git.research.acu.edu.au

+++

## Services available

* Consultation services:
	* Consultation and advice on research grants, especially data management
	* Assistance with software and tools
	* eResearch Analysis: How to improve efficiency in your work using technology

+++

## Services available

* Training:
	* Intersect training

---

## Where to get help

* Service Central
* Research office website
* Library website
* Intersect
* eResearch Mailbox: eResearch@acu.edu.au
